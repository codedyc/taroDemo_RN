import Taro, {Component} from '@tarojs/taro'
import {View, Button, Image} from '@tarojs/components'
import zhushaoming from '../../image/zhushaoming.png'
import './index.css'

export default class Index extends Component {

  // 头部设置
  config = {
    navigationBarTitleText: '2019.成都.测试大师课程'
  };

  componentWillMount() {
  }

  onClickToIntroduce = () => {
    Taro.navigateTo({
      url: '../Introduce/Introduce'
    })
  };

  render() {
    return (
      <View>
        <Image
          style={{width: '100%', height: '830px'}}
          src={zhushaoming}
          lazyLoad='true'
        />
        <Button className='start' onClick={this.onClickToIntroduce}>&gt;&gt;&gt;&gt;</Button>
      </View>
    )
  }
}
