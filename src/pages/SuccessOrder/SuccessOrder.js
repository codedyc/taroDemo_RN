import Taro, {Component} from '@tarojs/taro'
import {Image, View} from "@tarojs/components"
import {AtButton, AtCard} from "taro-ui";
import Header from '../../component/Header/header';
import QRCode from '../../image/QRcode.png';

export default class SuccessOrder extends Component {
  config = {
    navigationBarTitleText: '2019.成都.测试大师课程'
  };

  onClickToBack = () => {
    Taro.navigateTo({
      url: '../index/index'
    })
  }

  render() {
    return (
      <View>
        {Taro.getEnv() == 'WEB' ?
          <View style={{height: '2rem'}}><Header title='2019.成都.测试大师课程' onClick={this.onClickToBack}/></View> : ''
        }
        <View style={{textAlign:'center'}}>报名成功</View>
        <AtCard title='扫码加入.2019.成都.测试大师公开课微信群'>
          <Image src={QRCode} style={{width:'100%'}}/>
        </AtCard>
      </View>
    )
  }
}
