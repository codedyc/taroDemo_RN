const phoneRegular = {
  reg: /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/,
  msg: '手机号输入有误',
};

const idCardRegular = {
  reg: /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/,
  msg: '身份证号输入有误',
};
const emailRegular = {
  reg: /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/,
  msg: '邮箱号输入有误',
};
module.exports = {
  phoneRegular,
  idCardRegular,
  emailRegular,
};
