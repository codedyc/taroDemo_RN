import Taro, {Component} from '@tarojs/taro'
import {Button, Text, View} from "@tarojs/components"
import {AtMessage} from 'taro-ui'
import './CourseCount.css'
import Request, {connectURL} from '../../utils/request/Request';
import Config from '../../utils/api';
import {phoneRegular} from '../../utils/regular';


export default class CourseCount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    }
  }

  componentDidMount() {
    Request(connectURL(Config.API.allsourse, 'all'), {
      method: "GET", data: "",
    }).then((res) => {
      this.setState({
        data: res.data.data[0],
      })
    })
  }

  onClickToCourseSelect = () => {
    const {course, prices} = this.props;
    if (course.length === 0) {
      Taro.atMessage({
        message: '没有选课',
        type: 'warning',
      })
      return;
    }
    Taro.setStorageSync('data', course || []);
    Taro.setStorageSync('prices', prices || {});
    Taro.navigateTo({
      url: `../WhiteInfo/WhiteInfo`,
    })
  };

  addPhoneBlack = (phone) => {
    const reg = /(\d{3})(\d{4})(\d{4})/;
    if (phoneRegular.reg.test(phone)) {
      return phone.replace(reg, '$1 $2 $3');
    } else {
      return phoneRegular.msg;
    }
  }

  render() {
    const {prices} = this.props;
    const {data} = this.state;
    return (
      <View className='sourseAccount'>
        <AtMessage/>
        <View className='sourseAttention'>
          <View className='sourseAttentionTitle'>购票说明</View>
          <View className='sourseAttentionMessage'>
            <View>1、门票实名制（参会前两周可做信息变更）</View>
            <View>2、支持开具电子发票</View>
            <View>3、订票咨询：{this.addPhoneBlack(data.phone)}，</View>
            <View>E-mail:yam0909@163.com</View>
          </View>
        </View>
        <View className='sourseCount'>
          <View>原价：{prices.price}</View>
          <View>优惠：{prices.price ? '-' : ''}{prices.price - prices.selfPrice}</View>
          <View>实付：{prices.selfPrice}</View>
        </View>
        <View className='sourseSign'>
          <Button className='sourseSignButton' onClick={this.onClickToCourseSelect}>选课报名</Button>
        </View>
        <View className='organizer'>
          <View>承办方：</View>
          <View className='organizerMessage'>绵阳青目软件科技有限公司 绵阳启正软件测试有限公司</View>
        </View>
      </View>
    )
  }
}
// export default CourseSelectCard;
