import Taro, {Component} from '@tarojs/taro'
import {View} from '@tarojs/components'
import {AtIcon} from 'taro-ui';
import 'taro-ui/dist/style/components/flex.scss'
import 'taro-ui/dist/style/components/icon.scss';
import './header.css'

export default class Header extends Component {

  componentWillMount() {
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  componentDidShow() {
  }

  componentDidHide() {
  }

  render() {
    let {title, onClick} = this.props;
    return (
      <View className='at-row outer'>
        <AtIcon value='chevron-left' size='25' className='at-col at-col-1 icon' onClick={onClick}/>
        <text className='at-col at-col-8 inner'>{title}</text>
      </View>
    )
  }
}
