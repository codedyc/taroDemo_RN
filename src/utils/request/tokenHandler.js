import Taro from '@tarojs/taro'

const SessionStorageHandler = function () {
};
SessionStorageHandler.prototype = {
  getSessionObject () {
    return Taro.getStorageInfoSync();
  },
  getSessionByKey (key) {
    return Taro.getStorageSync(key)
  },
  setSessionByKey (key, value) {
    Taro.setStorageSync(key, value)
  },
  removeSessionByKey (key) {
    Taro.removeStorageSync(key)
  },
  clearSession () {
    Taro.clearStorageSync()
  },
};
export default new SessionStorageHandler();
