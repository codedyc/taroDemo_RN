import tokenHandler from './tokenHandler';

export const REQUEST_HEADER_LIST = {
  WITH_OUT_TOKEN_TYPE : 'WITH_OUT_TOKEN_TYPE',
  EXCEL_TYPE : 'EXCEL_TYPE',
  IMAGE_TYPE : 'IMAGE_TYPE',
  FILE_UPLOAD_TYPE : 'FILE_UPLOAD_TYPE',
  DEFAULT : 'DEFAULT',
};

// excell responseType blob??

export function getHeaderObject(typeStr) {
  let config = {};
  switch (typeStr) {
    case 'WITH_OUT_TOKEN_TYPE':
      config = {
        header : {
          'Content-Type' : 'application/json',
          Accept : 'application/json',
        },
        responseType : 'text',
      };
      break;
    case 'EXCEL_TYPE':
      config = {
        header : {
          token : tokenHandler.getSessionByKey('token'),
        },
        responseType : 'blob',
      };
      break;
    case 'IMAGE_TYPE':
      config = {
        header : {
          token : tokenHandler.getSessionByKey('token'),
          Accept : 'image/png',
        },
        responseType : 'arraybuffer',
      };
      break;
    case 'FILE_UPLOAD_TYPE':
      config = {
        header : {
          token : tokenHandler.getSessionByKey('token'),
          'Content-Type' : 'multipart/form-data',
        },
        responseType : 'text',
      };
      break;
    default:
      config = {
        header : {
          token : tokenHandler.getSessionByKey('token'),
          'Content-Type' : 'application/json',
          Accept : 'application/json',
        },
        responseType : 'text',
      };
  }
  return config;
}
