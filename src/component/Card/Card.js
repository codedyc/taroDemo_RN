import Taro, {Component} from '@tarojs/taro'
import {AtCard} from "taro-ui"
import './Card.css'


export default class Card extends Component {
  render() {
    const {title, note, extra, onClick} = this.props;
    return (
      <AtCard
        className='Card'
        title={title}
        note={note}
        extra={extra}
        onClick={onClick}
      >
        {this.props.children}
      </AtCard>
    )
  }
}
