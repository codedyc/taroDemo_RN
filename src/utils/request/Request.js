import TaroRequest from './TaroRequest';
import ipRemote from '../../utils/request/ip';

export default function Request(url, option, headerConfig) {
  return TaroRequest(url, option, headerConfig)
}

export function connectURL(api, interfaceType) {
  const reg = /\/?([a-zA-Z0-9]+)\/?/gm;
  api = api.replace(reg, '/$1');
  interfaceType = interfaceType.replace(reg, '/$1');
  return ipRemote() + api + interfaceType;
}
