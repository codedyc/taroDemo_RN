import Taro, {Component} from '@tarojs/taro'
import {AtCard, AtInput, AtButton, AtMessage} from "taro-ui"
import {Text, View} from '@tarojs/components'
import "taro-ui/dist/style/components/card.scss";
import "taro-ui/dist/style/components/input.scss";
import "taro-ui/dist/style/components/icon.scss";
import "taro-ui/dist/style/components/button.scss";
import "taro-ui/dist/style/components/loading.scss";
import "taro-ui/dist/style/components/message.scss"
import Request, {connectURL} from '../../utils/request/Request';
import Config from '../../utils/api';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.setState({
      username: 'admin',
      password: '666666',
      loading: false,
      disabled: false
    })
  }

  componentWillMount() {
  }

  componentDidMount() {
  }

  handleChangeUSN = (value) => {
    this.setState({
      username: value
    })
  };
  handleChangePWD = (value) => {
    this.setState({
      password: value
    })
  };
  onClick = (e) => {
    const {username, password} = this.state;
    let tempthis = this;
    this.setState({
      loading: true,
      disabled: true
    });
    Request(connectURL(Config.API.login, 'login'), {
      method: 'POST',
      data: {
        user: username,
        password: password
      }
    }).then(res => {
      if (res.data.data.token) {
        Taro.atMessage({
          'message': '登陆成功',
          'type': 'success',
        });
        Taro.setStorage({
          key: 'token',
          data: res.data.data.token
        })
          .then(res => {
            if (res.errMsg === 'setStorage:ok') {
              Taro.redirectTo({
                url: '/pages/index/index'
              })
              tempthis.setState({
                loading: false
              });
            } else {
              Taro.atMessage({
                'message': 'token存入失败，请检查硬盘内存剩余空间',
                'type': 'error',
              })
            }
          })
      } else {
        Taro.atMessage({
          'message': `登陆失败${res.errMsg}`,
          'type': 'error',
        });
      }
    })
  };

  render() {
    const {username, password} = this.state;
    return (
      <AtCard>
        <AtMessage/>
        <View><AtInput
          name='username'
          title='用户名'
          type='text'
          placeholder='用户名'
          value={username}
          onChange={this.handleChangeUSN.bind(this)}
        /></View>
        <View><AtInput
          name='password'
          title='密码'
          type='password'
          placeholder='密码'
          value={password}
          onChange={this.handleChangePWD.bind(this)}
        /></View>
        <View>
          <AtButton
            loading={this.state.loading}
            disabled={this.state.disabled}
            type='primary'
            size='normal'
            onClick={this.onClick.bind(this)}
          >登陆</AtButton>
        </View>
      </AtCard>
    )
  }
}
