import Taro, {Component} from '@tarojs/taro'
import {Image, Button, View} from "@tarojs/components"
import rubingsheng from '../../image/rubingsheng.png'
import Header from '../../component/Header/header';
import './introduce.css'


export default class Introduce extends Component {

  config = {
    navigationBarTitleText: '2019.成都.测试大师课程'
  };

  componentDidMount() {
  }

  onClickToCourseSelect = () => {
    Taro.navigateTo({
      url: '../CourseSelect/CourseSelect'
    })
  };

  onClickToBack = () => {
    Taro.navigateTo({
      url: '../index/index'
    })
  }

  render() {
    return (
      <View>
        {Taro.getEnv() == 'WEB' ?
          <View style={{height: '2rem'}}><Header title='2019.成都.测试大师课程' onClick={this.onClickToBack}/></View> : ''
        }
        <Image
          style={{width: '100%', height: '1030px'}}
          src={rubingsheng}
          lazyLoad='true'
        />
        <Button className='start' onClick={this.onClickToCourseSelect}>开始选课</Button>
      </View>
    )
  }
}
