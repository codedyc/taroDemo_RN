import Taro from '@tarojs/taro'
import {REQUEST_HEADER_LIST, getHeaderObject} from './requestHeader';

export default function TaroRequest(url, option, headerConfig = REQUEST_HEADER_LIST.DEFAULT) {
  const header = getHeaderObject(headerConfig);
  const {method, data} = option;
  const TaroRequestConfig = {
    url,
    method,
    data,
    ...header,
  };
  return Taro.request(TaroRequestConfig)
}
