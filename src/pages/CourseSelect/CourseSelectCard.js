import Taro, {Component} from '@tarojs/taro'
import {Button, Text, View} from "@tarojs/components"
import './CourseSelect.css'


export default class CourseSelectCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      courseNums: [],
      showButtons: true,
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    const {data} = this.props;
    let {price, list, deadLine: _deadLine, startTime: _startTime} = data;
    const date = new Date();
    const reg = /\./g;
    let _Today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    _deadLine = _deadLine.replace(reg, '-');
    _startTime = _startTime.replace(reg, '-');
    const Today = new Date(_Today),
      deadLine = new Date(_deadLine),
      startTime = new Date(_startTime);

    if (!(_startTime && _deadLine && Today.getTime() >= startTime.getTime() && Today.getTime() <= deadLine.getTime())) {
      this.setState({
        showButtons: false,
      })
    }
    list.forEach((item) => {
      item.courseNum = 0;
      item.price = price;
      item.courseId = item.Id;
    });
    this.setState({
      courseNums: data,
    })
  }

  config = {
    navigationBarTitleText: '2019.成都.测试大师课程'
  };

  addSourse = (item) => {
    const {func: {add}} = this.props;
    let {courseNums} = this.state;
    item.courseNum++;
    add(item);
    this.setState({
      courseNums: courseNums,
    })
  };

  delSourse = (item) => {
    const {func: {del}} = this.props;
    let {courseNums} = this.state;
    item.courseNum--;
    del(item);
    this.setState({
      courseNums: courseNums,
    })
  }

  render() {
    const {data} = this.props;
    const {showButtons} = this.state;
    return (
      <View className='sourseCard'>
        <View>
          <Text className='sourseTitle'>{data.ticket_time}</Text>
        </View>
        {data.list.map((item) => {
          return (
            <View key={item.id}>
              <View className='sourseFlex'>
                <View><Text className='soursePrice'>￥{data.price}</Text></View>
                {showButtons ? <View className='sourseButton'>
                  <Button size='mini' disabled={item.courseNum == 0 ? true : false}
                          onClick={() => this.delSourse(item)}>-</Button>
                  <Text className='sourseNums'>{item.courseNum}</Text>
                  <Button size='mini' onClick={() => this.addSourse(item)}>+</Button>
                </View> : ''}
              </View>
              <View className='sourseMessage'>
                <View>讲课老师<Text>{item.teacher}</Text></View>
                <View>讲课时间<Text>{item.startTime}~{item.endTime}</Text></View>
              </View>
            </View>
          )
        })}
        <View>
          <View className='sourseWell'><View className='specialWord'>团</View><View>单购两天课程五张以上享受团购优惠</View></View>
          <View className='sourseWell'><View className='specialWord'>全</View><View>全票四天课程{data.full_fare_tickets}元/人；五人以上享受团购优惠</View></View>
        </View>
      </View>
    )
  }
}

CourseSelectCard.defaultProps = {
  data: {
    list: ['1', '2'],
  }
};

// export default CourseSelectCard;
