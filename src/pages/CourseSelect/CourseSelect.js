import Taro, {Component} from '@tarojs/taro'
import {View} from "@tarojs/components"
import CourseSeletCard from './CourseSelectCard'
import CourseCount from './CourseCount'
import './CourseSelect.css'
import Request, {connectURL} from '../../utils/request/Request';
import Config from '../../utils/api';
import Header from '../../component/Header/header';

export default class CourseSelect extends Component {

  constructor() {
    super();
    this.state = {
      course: [],
      data: [],
      price: 0,
      selfPrice: 0,
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
    Request(connectURL(Config.API.course, "list"),
      {method: "GET", data: ""})
      .then(res => {
        this.setState({
          data: res.data.data,
        })
      })
  }

  config = {
    navigationBarTitleText: '2019.成都.测试大师课程'
  };

  addCourse = (data) => {
    const {course} = this.state;
    let _data = course.filter((value) => {
      return value.Id == data.Id;
    });
    if (_data.length === 0) {
      course.push(data);
      this.setState({
        course: course,
      }, () => this.countPrice(course))
    } else {
      _data.courseNum++;
      this.setState({
        course: course,
      }, () => this.countPrice(course))
    }
  };

  delSourse = (data) => {
    const {course} = this.state;
    let _data = course.filter((value) => {
      return value.Id == data.Id;
    });
    if (_data.length === 0) {
      course.push(data);
      this.setState({
        course: course,
      }, () => this.countPrice(course))
    } else {
      _data.courseNum--;
      this.setState({
        course: course,
      }, () => this.countPrice(course))
    }
  };

  countPrice = (course) => {
    let _price = 0,
      _selfprice = 0;
    // 价格计算
    course.forEach((item) => {
      _price += item.courseNum * item.price;
      _selfprice += item.courseNum * item.price - item.courseNum * 200;
    });
    this.setState({
      price: _price,
      selfPrice: _selfprice,
    })
  }

  onClickToBack = () => {
    Taro.navigateTo({
      url: '../Introduce/Introduce'
    })
  }

  render() {
    const {data, course, price, selfPrice} = this.state;
    const chooseCourseFunc = {
      add: this.addCourse,
      del: this.delSourse,
    };
    const prices = {
      price,
      selfPrice,
    }
    return (
      <View>
        {Taro.getEnv() == 'WEB' ?
          <View style={{height: '2rem'}}><Header title='2019.成都.测试大师课程' onClick={this.onClickToBack}/></View> : ''
        }
        <View className='sourseCard'>
          {data.map((item) => {
            return (
              <CourseSeletCard data={item} func={chooseCourseFunc} key={item.type}/>
            )
          })}
        </View>
        <CourseCount prices={prices} course={course}/>
      </View>
    )
  }
}
