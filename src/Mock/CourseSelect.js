export default {
  "errno": 0,
  "errmsg": "",
  "data": [{
    "Id": 1,
    "ticket_time": "提前购票优惠（7月10日前购票）",
    "type": 1,
    "price": 2400,
    "group_tickets": 2200,
    "full_fare_tickets": 4400,
    "full_fare_group_tickets": 4200,
    "list": [{"Id": 1, "type": 1, "teacher": "朱少民", "startTime": "2019.08.15", "endTime": "2019.08.16"}, {
      "Id": 2,
      "type": 1,
      "teacher": "茹炳晟",
      "startTime": "2019.08.17",
      "endTime": "2019.08.18"
    }]
  }, {
    "Id": 2,
    "ticket_time": "提前购票优惠（7月31日前购票）",
    "type": 2,
    "price": 2800,
    "group_tickets": 2500,
    "full_fare_tickets": 4600,
    "full_fare_group_tickets": 4400,
    "list": [{"Id": 3, "type": 2, "teacher": "朱少民", "startTime": "2019.08.15", "endTime": "2019.08.16"}, {
      "Id": 4,
      "type": 2,
      "teacher": "茹炳晟",
      "startTime": "2019.08.17",
      "endTime": "2019.08.18"
    }]
  }, {
    "Id": 3,
    "ticket_time": "现场标准票",
    "type": 3,
    "price": 3200,
    "group_tickets": 2800,
    "full_fare_tickets": 4800,
    "full_fare_group_tickets": 4400,
    "list": [{"Id": 5, "type": 3, "teacher": "朱少民", "startTime": "2019.08.15", "endTime": "2019.08.16"}, {
      "Id": 6,
      "type": 3,
      "teacher": "茹炳晟",
      "startTime": "2019.08.17",
      "endTime": "2019.08.18"
    }]
  }]
};

