import Taro, {Component} from '@tarojs/taro'
import {Button, Text, View, Radio, Image, RadioGroup, Label} from "@tarojs/components"
import "taro-ui/dist/style/components/input.scss";
import {AtInput, AtMessage, AtModal} from 'taro-ui'
import Card from '../../component/Card/Card'
import './WhiteInfo.css'
import WeiXin from '../../image/weixin.png';
import AliPay from '../../image/Alipay.png';
import Request, {connectURL} from "../../utils/request/Request";
import Config from "../../utils/api";
import {phoneRegular, emailRegular} from '../../utils/regular';
import Header from '../../component/Header/header';

const INPUT_ARRAY = [
  {
    title: '姓名',
    className: 'name',
    key: 'name',
    type: 'text',
    reg: '',
    require: true,
  },
  {
    title: '邮箱',
    className: 'email',
    key: 'email',
    type: 'text',
    reg: emailRegular,
    require: true,
  },
  {
    title: '手机号',
    className: 'telephone',
    key: 'telephone',
    type: 'text',
    reg: phoneRegular,
    require: true,
  },
  {
    title: '单位名称',
    className: 'enterpriseName',
    key: 'enterpriseName',
    type: 'text',
    reg: '',
    require: true,
  },
  {
    title: '职务',
    className: 'duty',
    key: 'duty',
    type: 'text',
    reg: '',
    require: false,
  },
  {
    title: '备注',
    className: 'memo',
    key: 'memo',
    type: 'text',
    reg: '',
    require: false,
  },
];

const payWay = [
  {
    value: '0',
    text: '银行支付',
    checked: true
  },
  {
    value: '1',
    text: '微信支付',
    checked: false,
  },
  {
    value: '2',
    text: '支付宝支付',
    checked: false
  },
];

export default class whiteInfo extends Component {

  constructor() {
    super();
    this.state = {
      prices: {},
      course: [],
      buyerData: {},
      inputData: {},
      showPayWay: "0",
      needReceipt: true,
      showModal: false,
      forceAdd: 0,
    }
  }

  componentDidMount() {
    Request(connectURL(Config.API.allsourse, 'all'), {
      method: "GET", data: "",
    }).then((res) => {
      this.setState({
        buyerData: res.data.data[0],
        course: Taro.getStorageSync('data'),
        prices: Taro.getStorageSync('prices'),
      })
    })
  }

  config = {
    navigationBarTitleText: '2019.成都.测试大师课程'
  };

  inputChange = (value, e) => {
    const {inputData} = this.state;
    const obj = {
      [e.currentTarget.id]: e.currentTarget.value,
    };
    this.setState({
      inputData: {...inputData, ...obj}
    })
    return value;
  };

  inputBlur = (reg, require, value, e) => {
    if (!value && require) {
      Taro.atMessage({
        'message': '该项不能为空',
        'type': 'error',
      });
      return;
    }
    if (value && reg.reg && !reg.reg.test(value)) {
      Taro.atMessage({
        'message': reg.msg,
        'type': 'error',
      });
      return;
    }
  }

  onChangePayWay = (value) => {
    this.setState({
      showPayWay: value.detail.value
    })
  };

  onChangeNeedReceipt = (value) => {
    this.setState({
      needReceipt: JSON.parse(value.detail.value),
    })
  };

  sendRegistration = () => {
    const {inputData, course, showPayWay, prices, forceAdd} = this.state;
    inputData.course = course;
    inputData.payment = parseInt(showPayWay);
    inputData.forceAdd = forceAdd;
    inputData.money = prices.selfPrice;
    if (this.judgeInputValue()) {
      Request(connectURL(Config.API.user, 'add'), {
        method: "POST", data: inputData,
      }).then((res) => {
        if (res.data.data.code == 0) {
          Taro.atMessage({
            'message': '购买成功',
            'type': 'success',
          });
          Taro.removeStorageSync('data');
          Taro.removeStorageSync('prices');
          Taro.redirectTo({
            url: '../SuccessOrder/SuccessOrder',
          })
        } else if (res.data.data.code == 1) {
          this.setState({
            showModal: true,
          })
        }
      })
    }
  }

  judgeInputValue = () => {
    const {inputData} = this.state;
    if (inputData.length === 0) {
      Taro.atMessage({
        'message': '请输入信息',
        'type': 'error',
      });
    }
    try {
      INPUT_ARRAY.forEach((item) => {
        if (item.require) {
          if (!inputData.hasOwnProperty(item.key)) {
            Taro.atMessage({
              'message': `请输入${item.title}信息`,
              'type': 'error',
            });
            throw new Error('信息错误');
          }
          const inputValue = inputData[item.key];
          if (inputValue.trim().length === 0) {
            Taro.atMessage({
              'message': `请输入${item.title}信息`,
              'type': 'error',
            });
            throw new Error('信息错误');
          }
          if (item.reg && !item.reg.reg.test(inputValue)) {
            Taro.atMessage({
              'message': `请输入正确的${item.title}信息`,
              'type': 'error',
            });
            throw new Error('信息错误');
          }
        }
      });
    } catch (e) {
      return false;
    }
    return true;
  }

  handleClose = () => {
    this.setState({
      showModal: false,
    })
  };

  handleConfirm = () => {
    this.setState({
      showModal: false,
      forceAdd: 1,
    }, () => {
      this.sendRegistration();
    })
  }

  onClickToBack = () => {
    Taro.navigateTo({
      url: '../CountSelect/CountSelect'
    })
  }

  render() {
    const {buyerData, prices, showPayWay, needReceipt, showModal, inputData} = this.state;
    return (
      <View>
        {Taro.getEnv() == 'WEB' ?
          <View style={{height: '2rem'}}><Header title='2019.成都.测试大师课程' onClick={this.onClickToBack}/></View> : ''
        }
        <Card>
          <View className='sourseTitle message'>{buyerData.title}</View>
          <View className='sourseTime message'>时间：{buyerData.start}~{buyerData.end}</View>
          <View className='sourseAddress message'>地址：{buyerData.address}</View>
        </Card>
        <Card>
          <View className='sourseBuyer'>购票人员信息</View>
          <AtMessage/>
          <View>
            {INPUT_ARRAY.map((inputConfig) => {
              const {title, className, type, key, reg, require} = inputConfig
              return (
                <View className='sourseBuyerInput' key={className}>
                  <View>{require ? <Text className='sourseBuyerInputStar'>*</Text> : ''}{title}</View>
                  <AtInput
                    name={className}
                    className={className}
                    type={type}
                    key={key}
                    onChange={this.inputChange}
                    onBlur={this.inputBlur.bind(this, reg, require)}
                    value={inputData[key] ? inputData[key] : ''}
                  />
                </View>
              )
            })}
          </View>
        </Card>
        <Card title='付款方式选择' extra={"￥" + prices.selfPrice}>
          <View className='payWay'>
            <RadioGroup onChange={this.onChangePayWay} className='RadioGroup'>
              {payWay.map((item, i) => {
                return (
                  <Label for={i} key={i}>
                    <Radio value={item.value} checked={item.checked}>{item.text}</Radio>
                  </Label>
                )
              })}
            </RadioGroup>
          </View>
          <View className='showPayWay'>
            {
              {
                '0': <View className='Bank'>
                  <View>户名：<Text>{buyerData.bankUserName}</Text></View>
                  <View>开户行：<Text>{buyerData.bank}</Text></View>
                  <View>账户：<Text>{buyerData.bankAccount}</Text></View>
                </View>,
                '1': <Image src={WeiXin}/>,
                '2': <Image src={AliPay}/>
              }[showPayWay]
            }
          </View>
          <View className='afterPayMemo'>
            <View>请将付款成功证明+参会人姓名发送至邮箱</View>
            <View>{buyerData.email}</View>
          </View>
        </Card>
        <Card>
          <RadioGroup onChange={this.onChangeNeedReceipt} className='RadioGroup'>
            <Text>是否需要发票</Text>
            <Label for='true' key='true'><Radio value='true' checked>需要</Radio></Label>
            <Label for='false' key='false'><Radio value='false'>不需要</Radio></Label>
          </RadioGroup>
          {
            needReceipt ?
              (<View>
                <View><Text className='sourseBuyerInputStar'>*</Text>购票方单位</View>
                <AtInput
                  name='ReceiptEnterpriseName'
                  className='ReceiptEnterpriseName'
                  onChange={this.inputChange}
                  value={inputData['ReceiptEnterpriseName'] ? inputData['ReceiptEnterpriseName'] : ''}
                />
                <View><Text className='sourseBuyerInputStar'>*</Text>统一社会信用代码</View>
                <AtInput
                  name='ReceiptSocialCode'
                  className='ReceiptSocialCode'
                  onChange={this.inputChange}
                  value={inputData['ReceiptSocialCode'] ? inputData['ReceiptSocialCode'] : ''}
                />
                <View className='needPayType'>发票内容：培训费</View>
              </View>) : ''
          }
          <View className='needPayPrice'>支付总额：￥{prices.selfPrice}</View>
          <Button className='submitRegistration' onClick={this.sendRegistration}>提交报名</Button>
        </Card>
        <AtModal
          isOpened={showModal}
          cancelText='取消'
          confirmText='确认'
          onClose={this.handleClose}
          onCancel={this.handleClose}
          onConfirm={this.handleConfirm}
          content='您已提交过信息，是否继续？'
        />
      </View>
    )
  }
}
