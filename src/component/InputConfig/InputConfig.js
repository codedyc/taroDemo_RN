import Taro, {Component} from '@tarojs/taro';
import {emailRegular, phoneRegular} from "../../utils/regular";
import {type} from '../../../src/common/common';

const inputConfig = [
  {
    title: '姓名',
    className: 'name',
    key: 'name',
    type: 'text',
    reg: '',
    require: true,
  },
  {
    title: '邮箱',
    className: 'email',
    key: 'eamil',
    type: 'text',
    reg: emailRegular,
    require: true,
  },
  {
    title: '手机号',
    className: 'telephone',
    key: 'telephone',
    type: 'text',
    reg: phoneRegular,
    require: true,
  },
  {
    title: '单位名称',
    className: 'enterpriseName',
    key: 'enterpriseName',
    type: 'text',
    reg: '',
    require: true,
  },
  {
    title: '职务',
    className: 'duty',
    key: 'duty',
    type: 'text',
    reg: '',
    require: false,
  },
  {
    title: '备注',
    className: 'memo',
    key: 'memo',
    type: 'text',
    reg: '',
    require: false,
  },
];

export default class InputConfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputArray: [],
    }
  }

  static propTypes = {
    inputArray: PropTypes.array,
  };

  static defaultProps = {
    inputArray: [],
  };
}
